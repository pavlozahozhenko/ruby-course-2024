class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :name, presence: true

  has_many :messages, dependent: :nullify
  has_one :settings, dependent: :destroy
  has_many :followings
  has_many :followers, through: :followings, class_name: 'User', foreign_key: :follower_id
  has_many :following_users,
           -> (user) { where(followings: {follower_id: user.id}) },
           through: :followings,
           class_name: 'User',
           foreign_key: :follower_id

  has_one_attached :profile_pic

  before_create :do_something
  before_destroy :cleanup

  def hello
    puts 'Hello, world!'
  end

  def admin?
    is_admin
  end

  private

  def do_something
    puts 'Doing something...'
  end

  def cleanup
    puts 'cleaning up...'
  end
end
