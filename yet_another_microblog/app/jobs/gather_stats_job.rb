class GatherStatsJob < ApplicationJob
  queue_as :default

  def perform(message_id)
    message = Message.find message_id
    message.gather_stats
  end
end
