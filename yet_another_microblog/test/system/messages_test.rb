require "application_system_test_case"

class MessagesTest < ApplicationSystemTestCase
  setup do
    @message = messages(:one)
    visit new_user_session_url
    @user = users(:testman)
    @user.save
    fill_in "user[email]", with: @user.email
    fill_in "user[password]", with: 'password'
    click_on 'Sign in'
  end

  def set_user_as_admin
    @user.update_column(:is_admin, true)
  end

  test "logging in" do
    assert_selector 'h1', text: 'Hello, world!'
  end

  test "visiting the index" do
    visit messages_url
    assert_selector "div", text: "Foo baz bar"
  end

  test "visiting the index with 12 messages" do
    10.times do
      create :message, user: @user
    end
    visit messages_url
    assert_equal all(".message").count, 12
  end

  test "should create message" do
    visit messages_url

    fill_in "message[text]", with: @message.text
    click_on "Create Message"

    assert_text @message.text
    assert_equal Message.last.text, @message.text
  end

  test "should update Message" do
    @message.update_column(:user_id, @user.id)
    visit message_url(@message)
    click_on "Edit this message", match: :first

    fill_in "message[text]", with: @message.text
    click_on "Update Message"

    assert_text "Message was successfully updated"
    click_on "Back"
  end

  test "should destroy Message" do
    set_user_as_admin
    visit message_url(@message)
    click_on "Destroy this message", match: :first

    assert_text "Message was successfully destroyed"
  end
end
