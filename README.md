# Ruby Course 2024

NaUKMA Ruby on Rails programming course 2024

### Announcements
* The project [deadline](https://gitlab.com/pavlozahozhenko/ruby-course-2024/-/tree/main/students?ref_type=heads#milestones) deadline is *17.04.2024*

### The Project
* [Requirements](https://gitlab.com/pavlozahozhenko/ruby-course-2024/-/tree/main/students?ref_type=heads#project-requirements-info)

### Repository
* [Repository 2024 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2024)

### Course Materials
* [Repository 2023 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2023)
* [Repository 2022 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2022)
* [Repository 2021 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2021)
* [Repository 2020 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2020)
* [Repository 2019 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2019)
* [Repository 2017 (BitBucket)](https://bitbucket.org/burius/ruby_course_2017)
* [Repository 2016 (BitBucket)](https://bitbucket.org/burius/ror_course)

### Useful Links
#### Ruby/Rails
* [Official Ruby documentation (core API)](https://ruby-doc.org/3.3.0/)
* [Ruby style guide](https://rubystyle.guide/) by bbatsov
* [Official Rails Guides](https://guides.rubyonrails.org/)
* [Rails API documentation](https://api.rubyonrails.org/)
* [Ruby Toolbox (ruby gems per category)](https://www.ruby-toolbox.com/)
