class App
  def call(env)
    puts "ENV: #{env.class} #{env}"
    if env['REQUEST_PATH'] == '/students'
      [
        200,
        {'Content-Type' => 'text/html'},
        ['Hello, <b>students!</b>']
      ]
    else
      [
        418,
        {'Content-Type' => 'application/json'},
        ['{"error": "teapot"}']
      ]
    end
  end
end

run App.new
