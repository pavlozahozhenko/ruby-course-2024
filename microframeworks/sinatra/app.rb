require 'sinatra'
require './db/connect'

get '/' do
  'App root'
end

get '/hello/:username' do |username|
  last_name = params[:last_name]
  full_name = "#{username} #{params[:last_name]}"
  erb :hello, locals: {name: full_name}
end

get '/students' do
  @students = DB[:students]
  erb :students
end

post '/students' do
  'Students POST'
end

put '/students' do
  'Students PUT'
end
