Sequel.migration do
  change do
    create_table(:students) do
      primary_key :id
      String :name, size: 255
      String :email, size: 100, null: false
    end
  end
end
